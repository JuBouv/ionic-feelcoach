import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// Boot
import { MyApp } from './app.component';

// Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { AddPage } from './../pages/add/add';
import { BlogPage } from './../pages/blog/blog';
import { BurgerPage } from './../pages/burger/burger';
import { FaqPage } from './../pages/faq/faq';
import { FavoritePage } from './../pages/favorite/favorite';
import { LogoutPage } from './../pages/logout/logout';
import { MessengerPage } from './../pages/messenger/messenger';
import { NavbarPage } from './../pages/navbar/navbar';
import { PaymentPage } from './../pages/payment/payment';
import { PhonePage } from './../pages/phone/phone';
import { ProfilePage } from './../pages/profile/profile';
import { SearchPage } from './../pages/search/search';
import { SubscribePage } from './../pages/subscribe/subscribe';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    AddPage,
    BlogPage,
    BurgerPage,
    FaqPage,
    FavoritePage,
    LogoutPage,
    MessengerPage,
    NavbarPage,
    PaymentPage,
    PhonePage,
    ProfilePage,
    SearchPage,
    SubscribePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    AddPage,
    BlogPage,
    BurgerPage,
    FaqPage,
    FavoritePage,
    LogoutPage,
    MessengerPage,
    NavbarPage,
    PaymentPage,
    PhonePage,
    ProfilePage,
    SearchPage,
    SubscribePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
